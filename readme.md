# Pig DAM Settings

## Overview

Settings job is to serve up configurations for our entire suite. It is divided up into:

- A [server](./src/server.js) that dynamically serves configurations for any known environment.
- A [CLI](./src/pigcmd.js) that is able to generate configurations

## Details

He is built based on four known environments: `development`, `local`, `production`, `staging` and `test`. He has the ability to generate configurations for any of these configurable environments for any and all of our services. 

## Problems

1. It is not configurable enough. It assumes things it should not such as:
   - a share being a file-system
   - no authentication or some single type of authentication
2. It is built over pre-defined environments. I don't think this should be a problem though it would be nice to reduce them down to a single configuration <a href="#configuration">see below</a>
3. It is more complicated than it needs to be - it both generates configurations and serves up configurations dynamically. It forces our applications to have both default configurations and overridable configurations loaded dynamically.
4. We manage more than one configuration at a time.

## Solution

I would like to divide a configuration up into two conceptual and, at least two, physical parts:
1. A _scaffold template_. 
2. A configuration with all of the information we need to create a configuration out of a template.

### Scaffold Template

A scaffold template is a template that outlines our configurations but does not go into any detail.  The details will come from his companion - the _configuration_

How many will we need? We will have to study this problem:

- Do we need one per each project?  Remember that each projects configuration is different. Each configuration contains in-depth details about itself and bare-essentials about his peers.  Perhaps we should always share in-depth info with every project in our suite?
- Do we want to break up the bits and peices to make the templates easier to manage and more easily mixed and matched?

### <a id="configuration"></a>Configuration

The _configuration_ has the brains. He tells settings what data to generate the templates with.  Data such as:
- the store (can the store exist anywhere but in a volume on file-system?)
  - type: local fs, volume, s3, etc.
  - path
- log
- server
- etc.

#### How will it work?
Each deployment will need a single configuration as each deployment will be built over a common `store`. These deployments will be able to manage 0 to `x` number of projects. But they all will live within the same store. If this cannot be true then the configuration of the store will have to exist outside of a configuration.

Let's assume the latter is true - all repos within a single deployment will live within the same store. So, what can we assume:
- only one configuration per project will be required for any single deployment. Which implies that one configuration will be needed deployment. What about different environments? We need the ability to build to different environments so that we may test `Pig DAM`. We _will_ need to accomodate this but very little will differ here between different environment configurations.
- configurations will need to be persisted and able to be generated:
  - when setting up the suite for development.
  - when we want to change the debug configuration:
    - service is run in a container
    - service is run locally
    - logging verbose
    - not logging verbose
    - etc.
  - when building the suite for deployment. The deployment will need to know which configuration to use.
  