# res/preset

## Overview
This is where we keep an assortment of preconfigured "params" to setting's configuration factory. See [pigcmd.js](../../src/pigcmd.js).
