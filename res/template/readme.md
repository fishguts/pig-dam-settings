# res/template

## Overview

This is where we keep configuration templates which [pigcmd.js](../../src/pigcmd.js) will use to build each module's configuration.

## Details

An application's configuration is built by pairing a [preset](../preset/readme.md) with a template. It's pretty simple and is almost effectively equivelant to `Object.assign(template, preset)`.

### `_library`

These are `key`/`value` pairs that may be defined once and used 0 or more times. The preset defines these values and the template tells the configuration factory which `_library` values he wants merged at build time.  

- See an example of a template implementation - [settings.json](settings.json)
- See an example of a preset implementation - [development.json](../preset/development.json)

## TODO

I removed `integration` from our settings schema. This makes the assumption that our services will never reach out to `settings` at runtime. I think this is good but will leave it until after christmas break.
