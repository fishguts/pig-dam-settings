/**
 * User: curtis
 * Date: 11/19/17
 * Time: 2:19 PM
 * Copyright @2017 by Xraymen Inc.
 *
 * This is the command interface to <link>./src/pigcmd.js</link>
 */

const command=require("pig-cmd").command;
const constant=require("../../common/constant");
const io=require("../../common/io");
const model_factory=require("../../data/factory");
const model_util=require("../../data/util");

/**
 * All actions supported by an implementing application
 * @type {Object<string, PigCliAction>}
 */
exports.ACTIONS={
	"gencfg": {
		args: "[project]",
		desc: "generates one or all project's settings using the specified configuration",
		/**
		 * @param {Command} command
		 * @returns {function(callback)}
		 */
		handler: (command)=>command._generateConfiguration.bind(command),
		/**
		 * @param {Array<string>} position
		 * @param {Object<string, string>} options
		 * @throws {Error} - if you want to fail validation
		 */
		validate: function(position, options) {
			if(position.length>0) {
				if(position.length>1) {
					throw new Error("incorrect number of position params");
				}
				if(!constant.isValidModule(position[0])) {
					throw new Error(`unknown application "${position[0]}"`);
				}
			}
		}
	}
};

/**
 * These are a complete list of all of the options we support on a command line:
 * @type {Object<string, PigCliAction>}
 */
exports.OPTIONS=[
	{
		actions: ["gencfg"],
		args: {
			count: 1,
			name: "environment"
		},
		desc: "one of our wellknown environments. Is mutally exlusive with -f and stdin",
		keys: {
			short: "e",
			long: "env"
		}
	},
	{
		actions: ["gencfg"],
		args: {
			count: 1,
			name: "preset-path"
		},
		desc: 'input "preset" file. Defaults to stdin',
		keys: {
			short: "f",
			long: "file"
		}
	}
];

/**
 * Interface into application specific commands
 */
class CommandInterface extends command.Command {
	constructor(action, options, position) {
		super(options);
		this.action=action;
		this.position=position;
		this.execute=exports.ACTIONS[this.action].handler(this);
	}

	_generateConfiguration(callback) {
		const _getSpecification=(_callback)=>{
			if(this.options.env) {
				process.nextTick(_callback, null, model_util.envToSpec(this.options.e));
			} else {
				const iocfg={
					json: true
				};
				if(this.options.file) {
					iocfg.filePath=this.options.file;
				} else {
					iocfg.stream=process.stdin;
				}
				(new io.Input(iocfg)).read(_callback);
			}
		};

		_getSpecification((error, spec)=>{
			if(error) {
				callback(error);
			} else {
				try {
					const project=this.position[0],
						configuration=(project)
							? model_factory.getProjectSettings(spec, project)
							: model_factory.getClusterSettings(spec);
					process.stdout.write(JSON.stringify(configuration, null, "\t"), callback);
				} catch(error) {
					callback(error);
				}
			}
		});
	}
}

exports.CommandInterface=CommandInterface;
