/**
 * User: curtis
 * Date: 11/22/17
 * Time: 11:48 PM
 * Copyright @2017 by Xraymen Inc.
 */

const http=require("pig-core").http;
const constant=require("./constant");
const model_factory=require("../data/factory");

/**
 * @type {module:pig-core/http}
 */
module.exports=Object.assign(module.exports, http);

/**
 * Factories for all routes in our suite
 */
exports.route={
	diag: {
		/**
		 * Gets ping path for a specified module or the current application
		 * @param {String} module optional. If not specified then defaults to application server
		 * @returns {Url}
		 */
		ping: function(module=undefined) {
			return (module)
				? model_factory.application.get().getModuleServerUrl(module, "diagnostics/ping")
				: model_factory.application.get().getServerUrl("diagnostics/ping");
		}
	},

	dam: {
		/**
		 * Create branch
		 * @param {String} projectId
		 * @param {String} sourceBranch
		 * @param {String} targetBranch
		 * @returns {{url:Url, body:Object}}
		 */
		branchCreate: function(projectId, sourceBranch, targetBranch) {
			return {
				url: model_factory.application.get().getModuleServerUrl(constant.module.DAM, `/vcs/branch/create/${projectId}/${targetBranch}`),
				body: {
					source: sourceBranch
				}
			};
		},
		/**
		 * Commit branch
		 * @param {String} projectId
		 * @param {String} branchName
		 * @returns {Url}
		 */
		branchCommit: (projectId, branchName)=>model_factory.application.get().getModuleServerUrl(constant.module.DAM,
			`/vcs/branch/commit/${projectId}/${branchName}`),
		/**
		 * Delete branch
		 * @param {String} projectId
		 * @param {String} branchName
		 * @returns {Url}
		 */
		branchDelete: (projectId, branchName)=>model_factory.application.get().getModuleServerUrl(constant.module.DAM,
			`/vcs/branch/delete/${projectId}/${branchName}`),
		/**
		 * Pull branch
		 * @param {String} projectId
		 * @param {String} branchName
		 * @returns {Url}
		 */
		branchPull: (projectId, branchName)=>model_factory.application.get().getModuleServerUrl(constant.module.DAM,
			`/vcs/branch/pull/${projectId}/${branchName}`),
		/**
		 * Reset branch
		 * @param {String} projectId
		 * @param {String} branchName
		 * @returns {Url}
		 */
		branchReset: (projectId, branchName)=>model_factory.application.get().getModuleServerUrl(constant.module.DAM,
			`/vcs/branch/reset/${projectId}/${branchName}`),
		/**
		 * Branch status
		 * @param {String} projectId
		 * @param {String} branchName
		 * @returns {Url}
		 */
		branchStatus: (projectId, branchName)=>model_factory.application.get().getModuleServerUrl(constant.module.DAM,
			`/vcs/branch/status/${projectId}/${branchName}`),
		/**
		 * branch history
		 * @param {String} projectId
		 * @param {String} branchName
		 * @param {String} relResPath
		 * @returns {Url}
		 */
		branchHistory: (projectId, branchName, relResPath=undefined)=>model_factory.application.get().getModuleServerUrl(constant.module.DAM,
			((relResPath)
				? `/vcs/branch/history/${projectId}/${branchName}/${relResPath}`
				: `/vcs/branch/history/${projectId}/${branchName}`)),

		/**
		 * Gets content
		 * @param {String} projectId
		 * @param {String} branchName
		 * @param {String} relResPath
		 * @returns {Url}
		 */
		contentGet: (projectId, branchName, relResPath)=>model_factory.application.get().getModuleServerUrl(constant.module.DAM,
			`/content/get/${projectId}/${branchName}/${relResPath}`),
		/**
		 * Adds content. Note this is just the url. Need to
		 * @param {String} projectId
		 * @param {String} branchName
		 * @param {String} relResPath
		 * @returns {Url}
		 */
		contentCreate: (projectId, branchName, relResPath)=>model_factory.application.get().getModuleServerUrl(constant.module.DAM,
			`/content/create/${projectId}/${branchName}/${relResPath}`),
		/**
		 * Update content
		 * @param {String} projectId
		 * @param {String} branchName
		 * @param {String} relResPath
		 * @returns {Url}
		 */
		contentUpdate: (projectId, branchName, relResPath)=>model_factory.application.get().getModuleServerUrl(constant.module.DAM,
			`/content/update/${projectId}/${branchName}/${relResPath}`),
		/**
		 * Update content
		 * @param {String} projectId
		 * @param {String} branchName
		 * @param {String} relResPath
		 * @returns {Url}
		 */
		contentMove: (projectId, branchName, relResPath)=>model_factory.application.get().getModuleServerUrl(constant.module.DAM,
			`/content/move/${projectId}/${branchName}/${relResPath}`),
		/**
		 * Update content
		 * @param {String} projectId
		 * @param {String} branchName
		 * @param {String} relResPath
		 * @returns {Url}
		 */
		contentDelete: (projectId, branchName, relResPath)=>model_factory.application.get().getModuleServerUrl(constant.module.DAM,
			`/content/delete/${projectId}/${branchName}/${relResPath}`),

		/**
		 * Update search
		 * @param {String} projectId
		 * @param {String} branchName
		 * @param {String} relResPath
		 * @returns {Url}
		 */
		contentSearch: (projectId, branchName, relResPath=undefined)=>model_factory.application.get().getModuleServerUrl(constant.module.DAM,
			(relResPath)
				? `/content/search/${projectId}/${branchName}/${relResPath}`
				: `/content/search/${projectId}/${branchName}`),

		/**
		 * Create a project
		 * @param {String} name
		 * @param {Object} options
		 * @returns {{url:Url, body:Object}}
		 */
		projectCreate: function(name, options=undefined) {
			return {
				url: model_factory.application.get().getModuleServerUrl(constant.module.DAM, "project/create"),
				body: Object.assign({
					name: name
				}, options)
			};
		},
		/**
		 * Update a project
		 * @param {String} projectId
		 * @returns {Url}
		 */
		projectUpdate: (projectId)=>model_factory.application.get().getModuleServerUrl(constant.module.DAM, `project/update/${projectId}`),
		/**
		 * Delete a project
		 * @param {String} projectId
		 * @returns {Url}
		 */
		projectDelete: (projectId)=>model_factory.application.get().getModuleServerUrl(constant.module.DAM, `project/delete/${projectId}`)
	},

	factory: {
		/**
		 * get exif metadata about a file
		 * @param {String} relativeToStoreUrl
		 * @returns {Url}
		 */
		exifMetadataGet: (relativeToStoreUrl)=>model_factory.application.get().getModuleServerUrl(constant.module.FACTORY,
			`/metadata/exif/${relativeToStoreUrl}`),
		/**
		 * get AI metadata about a file
		 * @param {String} relativeToStoreUrl
		 * @returns {Url}
		 */
		aiMetadataGet: (relativeToStoreUrl)=>model_factory.application.get().getModuleServerUrl(constant.module.FACTORY,
			`/metadata/ai/${relativeToStoreUrl}`)
	},

	settings: {
		/**
		 * Get application settings
		 * @param {String} appName
		 * @param {String} nodenv
		 * @returns {Url}
		 */
		applicationGet: function(appName, nodenv=undefined) {
			const application=model_factory.application.get();
			return application.getModuleServerUrl(constant.module.SETTINGS, `/settings/${appName}/${nodenv || application.nodenv}`);
		},
		/**
		 * Get the entire cluster's settings
		 * @param {String} nodenv
		 * @returns {Url}
		 */
		clusterGet: function(nodenv=undefined) {
			const application=model_factory.application.get();
			return application.getModuleServerUrl(constant.module.SETTINGS, `/settings/cluster/${nodenv || model_factory.application.get().nodenv}`);
		}
	}
};
