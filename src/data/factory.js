/**
 * User: curtis
 * Date: 11/10/17
 * Time: 10:49 PM
 * Copyright @2017 by Xraymen Inc.
 */

const _=require("lodash");
const fs=require("fs-extra");
const {PigError}=require("pig-core").error;
const notification=require("pig-core").notification;
const util=require("pig-core").util;
const db_model=require("./model");
const constant=require("../common/constant");


const storage={
	/**
	 * Manages our instances per application: application.local, application.dev, etc.
	 */
	application: {},
	nodenv: (process.env.NODE_ENV || constant.nodenv.LOCAL)
};


/**
 * Gets/Sets application singleton
 */
exports.application={
	/**
	 * Gets a configuration for the specified application
	 * @param {Object} overrides
	 * @param {String} overrides.nodenv
	 * @param {String} overrides.mode
	 * @returns {Application}
	 */
	get: function(overrides=undefined) {
		let nodenv=storage.nodenv;
		if(_.has(overrides, "nodenv")) {
			nodenv=overrides.nodenv;
			overrides=_.omit(overrides, "nodenv");
		}
		if(storage.application[nodenv]) {
			// the application has already been configured nonetheless we will take overrides
			if(overrides) {
				_.merge(storage.application[nodenv].raw.application, overrides);
			}
		} else {
			exports.application.set(fs.readJSONSync(`./res/configuration/${nodenv}/settings.json`), overrides);
		}
		return storage.application[nodenv];
	},

	/**
	 * Sets a configuration for the specified application. The only time this should need to be called outside of the factory
	 * is when we are retrieving settings from our settings server
	 * @param {Object} settings
	 * @param {Object} overrides
	 * @returns {Application}
	 */
	set: function(settings, overrides=undefined) {
		const nodenv=settings.env.name;
		// help keep the differences between apps easier to manage by setting up an "application" domain.
		// Once this is set then the differences in Application per module will be minimized
		settings.application=settings.module.settings;
		_.merge(settings.application, overrides);
		storage.application[nodenv]=new db_model.Application(settings);
		this._applicationToEnvironment(storage.application[nodenv]);
		if(nodenv===storage.nodenv) {
			notification.emit(constant.event.SETTINGS_LOADED);
		}
		return storage.application[nodenv];
	},

	/**
	 * Aligns the environment to our application
	 * @param {Application} application
	 * @private
	 */
	_applicationToEnvironment: function(application) {
		process.env.DEBUG=application.debug.enabled;
		process.env.VERBOSE=application.debug.verbose;
	}
};

/**
 * Gets the whole cluster's configuration in detail
 * @param {String} spec
 * @returns {Object}
 * @throws {PigError}
 */
exports.getClusterSettings=function(spec) {
	try {
		const settings=require("../../res/template/settings");
		// make the "library" substitutions
		_.each(settings.module, function(module) {
			_.each(module._library, function(key) {
				_.set(module, key, spec.settings._library[key]);
			});
		});
		// merge in whatever else has been included as overrides
		_.merge(settings, _.omit(spec.settings, "_library"));
		// remove our own internal properties
		return util.scrubObject(settings, {
			recursive: true,
			removables: (value, key)=>key[0]==="_"
		});
	} catch(error) {
		throw new PigError({
			error,
			message: "attempt to render the cluster's settings failed"
		});
	}
};

/**
 * Gets the whole cluster's configuration but detail is only specified for the application being requested.
 * @param {String} spec - the configuration to apply to our template
 * @param {String} project
 * @returns {Object}
 * @throws {PigError}
 */
exports.getProjectSettings=function(spec, project) {
	if(constant.isValidModule(project)) {
		const result=exports.getClusterSettings(spec);
		// let's clean him up. The app for whom we are generating this configuration cares about everything
		// but regarding the rest of our ecosystem he only cares about the server....and anything else - we leave it in there.
		_(result.module)
			.keys()
			.xor([project])
			.each(function(moduleKey) {
				_(result.module[moduleKey])
					.keys()
					.xor(["database", "indices", "name", "server"])
					.each((propertyKey)=>delete result.module[moduleKey][propertyKey]);
			});
		return result;
	} else {
		throw new PigError({
			details: `unknown project "${project}"`,
			message: "unable to build settings"
		});
	}
};
