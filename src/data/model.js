/**
 * User: curtis
 * Date: 11/10/17
 * Time: 10:49 PM
 * Copyright @2017 by Xraymen Inc.
 */


const constant=require("../common/constant");
const http=require("../common/http");


/**
 * Base class. All work with blob of data.
 */
class Model {
	/**
	 * @param {Object} data
	 */
	constructor(data) {
		this._data=data;
	}

	/**
	 * Gets the raw object that is this object
	 * @returns {Object}
	 */
	get raw() {
		return this._data;
	}
}

/**
 * Application information. Largely a server for our application configuration info with an application jobQueue.
 */
class Application extends Model {
	constructor(data) {
		super(data);
	}

	/**** Application Environment Information ****/
	/**
	 * NODE_ENV or our own default
	 * @returns {String}
	 */
	get nodenv() { return this._data.env.name; }
	get isLowerEnv() { return !this.isUpperEnv; }
	get isUpperEnv() { return this.nodenv===constant.nodenv.STAGING || this.nodenv===constant.nodenv.PRODUCTION; }

	/**
	 * Debug information
	 * @returns {{enabled:Boolean, verbose:Boolean}}
	 */
	get debug() { return this._data.application.debug; }

	/**
	 * Log configuration information
	 * @returns {{level:String, transports:[{type:String, overrides:Object}]}}
	 */
	get log() { return this._data.application.log; }

	/**** Application Specific Details ****/
	/**
	 * Mode in which this guy is running. See <code>constant.application.mode</code>
	 * @returns {String}
	 */
	get mode() { return this._data.application.mode; }
	/**
	 * Name of the project
	 */
	get name() { return this._data.application.name; }
	/**
	 * Application's own server details
	 * @returns {{host:string, port:Number}}
	 */
	get server() { return this.getModuleServer(this._data.application.name); }
	/**
	 * Builds uri for current host. Entirely for logging
	 * @param {String} path
	 * @returns {Url}
	 */
	getServerUrl(path=undefined) { return this.getModuleServerUrl(this._data.application.name, path); }

	/**
	 * Gets metadata information about our servers. See constant.module for module names
	 * @param {String} name
	 * @returns {{name:string, server:Object}}
	 */
	getModule(name) { return this._data.module[name]; }
	/**
	 * Gets the server host and port. It determines whether to use the container properties or other depending on whether this
	 * client is in a docker network or not. We track this via the environment variable (see Dockerfile) HOST_ENV
	 * @param {String} name
	 * @returns {{host:String, port:Number}}
	 */
	getModuleServer(name) {
		return (process.env.HOST_ENV==="docker")
			? this._data.module[name].server.docker
			: this._data.module[name].server.debug;
	}
	/**
	 * Builds a Url to the target module
	 * @param {String} name
	 * @param {String} path
	 * @returns {Url}
	 */
	getModuleServerUrl(name, path=undefined) {
		return new http.Url(this.getModuleServer(name), path);
	}

	/**
	 * Do we want to use our local configuration files or do we want to reach out to our "settings" service
	 * @returns {Boolean}
	 */
	useLocalConfiguration() { return this._data.application.useLocalConfiguration; }
}

exports.Application=Application;
