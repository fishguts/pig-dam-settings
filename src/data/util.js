/**
 * User: curtis
 * Date: 2018-12-22
 * Time: 12:59
 * Copyright @2018 by Xraymen Inc.
 */

const {PigError}=require("pig-core").error;
const file=require("pig-core").file;

/**
 * Bridges a gap between old and new which is to convert an environment (old) to a preset (new)
 * @param {string} env
 * @returns {Object}
 * @throws {PigError}
 */
exports.envToSpec=function(env) {
	try {
		return file.readToJSONSync(`./res/preset/${env}.json`);
	} catch(error) {
		throw new PigError({
			error,
			message: `unable to translate environment "${env}" into a preset`
		});
	}
};

