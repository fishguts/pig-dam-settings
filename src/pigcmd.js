#!/usr/bin/env node

/**
 * User: curtis
 * Date: 11/19/17
 * Time: 1:22 PM
 * Copyright @2017 by Xraymen Inc.
 *
 * A command line interface for issuing commands in this application.
 */

const cli=require("pig-cmd").cli;
const pigcmd=require("./command/pigcmd");
const constant=require("./common/constant");
const {SetupEnvironmentCommand}=require("./command/setup");

cli.run(pigcmd, {
	setupCommand: new SetupEnvironmentCommand({
		mode: constant.application.mode.CLI
	})
});
