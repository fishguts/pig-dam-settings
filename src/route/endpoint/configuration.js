/**
 * User: curtis
 * Date: 11/21/17
 * Time: 8:30 PM
 * Copyright @2017 by Xraymen Inc.
 */

const response=require("pig-cmd").http.response;
const {Route}=require("pig-cmd").route;
const model_factory=require("../../data/factory");
const model_util=require("../../data/util");


/**
 * Retrieves a cluster configuration by environment/preset
 */
class ClusterConfigurationGet extends Route {
	get schema() {
		return "./res/schema/route/cluster-get.json#request";
	}

	execute(callback) {
		const preset=model_util.envToSpec(this.req.params.environment),
			settings=model_factory.getClusterSettings(preset),
			command=new response.HttpSendJsonCommand(this.res, {
				inputData: settings
			});
		command.execute(callback);
	}
}

/**
 * Retrieves a cluster configuration using the body of this request
 */
class ClusterConfigurationPut extends Route {
	get schema() {
		return "./res/schema/route/cluster-put.json#request";
	}

	execute(callback) {
		const settings=model_factory.getClusterSettings(this.req.body),
			command=new response.HttpSendJsonCommand(this.res, {
				inputData: settings
			});
		command.execute(callback);
	}
}

/**
 * Gets a single project's configuration
 */
class ProjectConfigurationGet extends Route {
	get schema() {
		return "./res/schema/route/project-get.json#request";
	}

	execute(callback) {
		const project=this.req.params.project,
			spec=model_util.envToSpec(this.req.params.environment),
			settings=model_factory.getProjectSettings(spec, project),
			command=new response.HttpSendJsonCommand(this.res, {
				inputData: settings
			});
		command.execute(callback);
	}
}

/**
 * Gets a single project's configuration with the spec in the body
 */
class ProjectConfigurationPut extends Route {
	get schema() {
		return "./res/schema/route/project-put.json#request";
	}

	execute(callback) {
		const settings=model_factory.getProjectSettings(this.req.body, this.req.params.project),
			command=new response.HttpSendJsonCommand(this.res, {
				inputData: settings
			});
		command.execute(callback);
	}
}

module.exports={
	ClusterConfigurationGet,
	ProjectConfigurationGet,
	ClusterConfigurationPut,
	ProjectConfigurationPut
};
