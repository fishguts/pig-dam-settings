/**
 * User: curtis
 * Date: 11/21/17
 * Time: 8:31 PM
 * Copyright @2017 by Xraymen Inc.
 */

const response=require("pig-cmd").http.response;
const {Route}=require("pig-cmd").route;
const http=require("../../common/http");
const model_factory=require("../../data/factory");

/**
 * Sends an okay status back to our mother-ship
 */
class PingRoute extends Route {
	execute(callback) {
		const queue=this.createCommandQueue();
		queue.addCommand(new response.HttpSendStatusCommand(this.res, {
			statusCode: http.status.code.OK
		}));
		queue.execute(callback);
	}
}

/**
 * Sends configuration of the current environment (with environment info)
 */
class EnvironmentRoute extends Route {
	execute(callback) {
		const queue=this.createCommandQueue(),
			application=model_factory.application.get();
		queue.addCommand(new response.HttpSendJsonCommand(this.res, {
			inputData: application.raw
		}));
		queue.execute(callback);
	}
}

module.exports={
	EnvironmentRoute,
	PingRoute
};
