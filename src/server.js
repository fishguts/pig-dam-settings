/**
 * User: curtis
 * Date: 11/21/17
 * Time: 9:24 PM
 * Copyright @2017 by Xraymen Inc.
 */

const {ExpressServerCommand}=require("pig-cmd").express;
const {Command}=require("pig-cmd").command;
const {SeriesQueue}=require("pig-cmd").queue;
const {SetupEnvironmentCommand}=require("./command/setup");
const data_factory=require("./data/factory");
const route_factory=require("pig-cmd").route.factory;
const route_diagnostics=require("./route/endpoint/diagnostics");
const route_configuration=require("./route/endpoint/configuration");


/**
 * We want to make sure the application environment is loaded before we do anything with it which is why we isolate the
 * commands in the startup command - they are all run after the environment has been loaded
 */
class StartupCommand extends Command {
	execute(callback) {
		const application=data_factory.application.get(),
			queue=new SeriesQueue({verbose: true}),
			server=new ExpressServerCommand({
				name: application.name,
				nodenv: application.nodenv,
				port: application.server.port
			});
		configureServerRoutes(server.router);
		queue.addCommand(server);
		queue.execute(callback);
	}
}

/**
 * @param {Router} router
 */
function configureServerRoutes(router) {
	/**** diagnostic routes ****/
	router.get("/diagnostics/env", route_factory.create(route_diagnostics.EnvironmentRoute));
	router.get("/diagnostics/ping", route_factory.create(route_diagnostics.PingRoute));

	/**** slave routes to our minions ****/
	router.put("/settings/cluster", route_factory.create(route_configuration.ClusterConfigurationPut));
	router.get("/settings/cluster/:environment", route_factory.create(route_configuration.ClusterConfigurationGet));
	router.put("/settings/:project", route_factory.create(route_configuration.ProjectConfigurationPut));
	router.get("/settings/:project/:environment", route_factory.create(route_configuration.ProjectConfigurationGet));
}

function startup() {
	const series=new SeriesQueue({verbose: true});
	series.addCommand(new SetupEnvironmentCommand());
	series.addCommand(new StartupCommand());
	series.execute(function(error) {
		if(error) {
			process.exit(1);
		}
	});
}


startup();
