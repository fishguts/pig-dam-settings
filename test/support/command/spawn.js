/**
 * User: curt
 * Date: 12/01/2017
 * Time: 5:45 PM
 */

const path=require("path");
const log=require("pig-core").log;
const proxy=require("pig-core").proxy;
const {Command}=require("pig-cmd").command;
const request=require("pig-cmd").http.request;
const {NodeServer}=require("pig-cmd").support.spawn;
const factory=require("../factory");
const {setupEnvironment}=require("../../support/setup");
const http=require("../../../src/common/http");

/**
 * His job is to make sure a server is running.  Supported options:
 *  - to discover an existing server and use it if one is found
 *  - to discover an existing server and start up a new one if none is found
 *  - to not look for a server and try to attempt to start up a new one
 * @type {Command} StartTestServerCommand
 */
class StartTestServerCommand extends Command {
	/**
	 * @param {Application} application
	 * @param {Boolean} discover - look for an existing server and use it if one is found
	 * @param {Boolean} spawnVerbose - log spawned server debug std-input or not. Will always log spawned warnings and errors
	 * @param {Boolean} stubLog - will stub log locally. Has no effect on the output of the spawned server.
	 * @param {Object} options - additional params
	 */
	constructor({
		application=factory.application.get(),
		discover=true,
		spawnVerbose=factory.application.get().debug.verbose,
		stubLog=false,
		...options
	}) {
		setupEnvironment(application);
		super({discover, spawnVerbose, stubLog, ...options});
		this._application=application;
	}

	execute(callback) {
		const _runServer=()=>{
			log.debug(this.toString(`starting "${this._application.name}" server`));
			NodeServer.getSingleton({
				serverRoot: path.resolve("."),
				serverPath: "./src/server.js",
				verbose: this.getOption("spawnVerbose")
			}).start(callback);
		};

		if(this.getOption("stubLog")) {
			proxy.log.stub(["error"]);
		}
		if(!this.getOption("discover")) {
			_runServer();
		} else {
			const pingUrl=http.route.diag.ping();
			log.debug(this.toString(`entering discover mode - pinging ${this._application.getServerUrl()}`));
			new request.HttpGetRequestCommand(pingUrl).execute((error)=>{
				if(error) {
					_runServer();
				} else {
					log.debug(this.toString(`ping acknowledged - using ${this._application.getServerUrl()}`));
					callback();
				}
			});
		}
	}
}

/**
 * Finds one of our servers running and registered in <code>NodeServer.getSingleton()</code> and stops it.
 * If he doesn't find one then nothing is stopped.
 * @type {Command} StopTestServerCommand
 */
class StopTestServerCommand extends Command {
	/**
	 * @param {Application} application
	 * @param {Boolean} unstubLog - remove stub setup by <code>StartTestServerCommand</code>
	 * @param {Object} options - additional params
	 */
	constructor({
		application=factory.application.get(),
		unstubLog=false,
		...options
	}) {
		super({unstubLog, ...options});
		this._application=application;
	}

	execute(callback) {
		const server=NodeServer.getSingleton({
			serverRoot: path.resolve("."),
			serverPath: "./src/server.js"
		});
		if(this.getOption("unstubLog")) {
			proxy.log.unstub();
		}
		if(!server.isRunning()) {
			process.nextTick(callback);
		} else {
			log.debug(this.toString(`killing "${this._application.name}" server`));
			server.stop((error)=>{
				if(error) {
					this.logError(error);
				}
				callback(error);
			});
		}
	}
}

module.exports={
	StartTestServerCommand,
	StopTestServerCommand
};
