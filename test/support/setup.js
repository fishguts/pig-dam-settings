/**
 * User: curtis
 * Date: 2018-12-15
 * Time: 22:44
 * Copyright @2018 by Xraymen Inc.
 */

const test_factory=require("./factory");
const {setupEnvironment}=require("../../src/common/setup");

/**
 * Brings together configuration and an instance of a test factory's application
 * @param {Application} application
 */
exports.setupEnvironment=function(application=test_factory.application.get()) {
	setupEnvironment(application);
};
