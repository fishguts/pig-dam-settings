/**
 * User: curtis
 * Date: 2018-12-22
 * Time: 13:08
 * Copyright @2018 by Xraymen Inc.
 */

const _=require("lodash");
const assert=require("assert");
const model_util=require("../../../src/data/util");

describe("model.util", function() {
	describe("envToPreset", function() {
		it("should properly translate and load a known environment", function() {
			const preset=model_util.envToSpec("development");
			assert.ok(_.isPlainObject(preset));
		});
	});

	describe("envToPreset", function() {
		it("should throw an exception if an environment is not known", function() {
			assert.throws(model_util.envToSpec.bind(null, "unknown"), error=>{
				return error.message===`unable to translate environment "unknown" into a preset`;
			});
		});
	});
});
