/**
 * User: curtis
 * Date: 12/08/17
 * Time: 10:01 PM
 * Copyright @2017 by Xraymen Inc.
 */

const _=require("lodash");
const assert=require("assert");
const constant=require("../../../src/common/constant");
const model_factory=require("../../../src/data/factory");

describe("db.factory", function() {
	describe("getClusterSettings", function() {
		it("should successfully load our debug environment", function() {
			const spec=require("../../../res/preset/development"),
				result=model_factory.getClusterSettings(spec);
			assert.ok(_.isPlainObject(result));
		});
	});

	describe("getProjectSettings", function() {
		it("should throw an exception if application is not known", function() {
			const spec=require("../../../res/preset/development");
			assert.throws(model_factory.getProjectSettings.bind(null, spec, "unknown"));
		});

		it("should successfully load our a known application and environment", function() {
			const spec=require("../../../res/preset/development"),
				result=model_factory.getProjectSettings(spec, constant.module.DAM);
			assert.ok(_.isPlainObject(result));
		});
	});
});
